FROM openjdk:11.0.8-jre-slim
COPY build/parking-0.0.1.jar parking.jar
ENTRYPOINT ["java", "-jar", "parking.jar"]