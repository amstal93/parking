package com.kt.mvc.parking.view;

import com.kt.mvc.parking.controller.parkingController.ParkingController;
import com.kt.mvc.parking.controller.parkingController.ParkingMVC;
import com.kt.mvc.parking.controller.printer.PrinterController;
import com.kt.mvc.parking.controller.printer.PrinterMVC;
import com.kt.mvc.parking.controller.scanner.ScannerController;
import com.kt.mvc.parking.controller.scanner.ScannerMVC;
import com.kt.mvc.parking.model.Parking;
import com.kt.mvc.parking.model.parkingSpace.ParkingSpaceDAO;
import com.kt.mvc.parking.model.vehicles.HistoryVehicleDAO;
import com.kt.mvc.parking.model.vehicles.VehicleDAO;

public class ParkingView implements ParkingMVC.View {

    private ScannerMVC.Controller scannerController = new ScannerController();
    private PrinterMVC.Controller printerController = new PrinterController();

    private Parking parking =  Parking.create(new ParkingSpaceDAO(), new VehicleDAO(),
            new HistoryVehicleDAO(), printerController);

    @Override
    public void start() {

        ParkingController parkingController = new ParkingController(parking, scannerController, printerController);
        parkingController.manageParking();

    }
}
